const eventItems = document.querySelectorAll(".js-nav-item");
const timelineItems = document.querySelectorAll(".js-section");

const navCheckbox = document.querySelector(".js-nav-checkbox");

const isSelected = "is-selected";
const isVisible = "is-shown";

function toggleClass(elements, index, className) {
    elements.forEach((element) => {
        element.classList.remove(className);
    });
    elements[index].classList.add(className);
}

eventItems.forEach((eventItem, index) => {
    eventItem.addEventListener('click', () => {
        toggleClass(eventItems, index, isSelected);
        toggleClass(timelineItems, index, isVisible);

        // hides menu on mobile only for checkbox hack
        navCheckbox.checked = false;

        // scrolls back to top
        window.scroll({
            top: 0, 
            behavior: 'smooth' 
          });
    });
});