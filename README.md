# Vera Nadler

Website for Vera Nadler Coaching.

![Screenshot of seecafe-goeppingen.de](./screenshot.png)

## Getting Started

```
$ git clone https://github.com/simonh9134/see_cafe.git
  cd vera_nadler
  npm install
  gulp dev
```

## Built With

* [Gulp](https://gulpjs.com/)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Atom](https://atom.io/)

## Authors

* **Oliver Staudenmayer** - *Creative design* - [lucadesign.de](http://www.lucadesign.de/)
